const fs = require("fs");

exports.readFile= (path, d) => {
    /* Reads in file and returns promise based on status of reading file */
    return new Promise((resolve, reject) => {
        fs.readFile(path, "utf8", (err, data) => {
            if (err) {
                console.error("Error reading file = ", err);
                reject({});
            }
            console.log(data)
            d = data;
        })
    })
}

exports.processFile = (file) => {
/*
  var lineReader = require('readline').createInterface({
    input: fs.createReadStream('file.in')
  });

  lineReader.on('line', function (line) {
    console.log('Line from file:', line);
  });
*/
}

exports.writeFile = (records, output) => {
    /* Writes and saves the hash table to a file */

    //convert hash table into a string
    let strRecords = "Test";

    //save file to disk
    fs.writeFile(output, strRecords, function (err) {
        if (err) {
            return console.error(err);
        }
        console.log("File saved successfully");
    });
}
