var app = angular.module('StarterApp', ['ngRoute', 'ngMaterial']); 
app.config(function($routeProvider) {
  $routeProvider
  .when("/home", {
    templateUrl : "home.html"
  })
  .when("/settings", {
    templateUrl : "settings.html"
  });
});
app.directive('fileModel', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function() {
        scope.$apply(function() {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);
app.service('fileUpload', ['$http', function ($http) {
  this.uploadFileToUrl = function(file, uploadUrl) {
    var fd = new FormData();
    fd.append('file', file);

    $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': 'text/plain'}
    })
      .success(function() {
      })
      .error(function() {
      });
  }
}]);
app.controller('AppCtrl', ['$scope', '$http','$filter', 'fileUpload', '$window', function($scope, $http, $filter, $mdDialog, fileUpload){
  $scope.toggleSearch = false;
  $scope.headers = [
    {
      name:'',
      field:'thumb'
    },{
      name: 'Name', 
      field: 'name'
    },{
      name:'Description', 
      field: 'description'
    },{
      name: 'Last Modified', 
      field: 'last_modified'
    }
  ];
  $http.get('./test.json').success(function(data) {
    $scope.content = data;
  });
  $scope.custom = {name: 'bold', description:'grey',last_modified: 'grey'};
  $scope.sortable = ['name', 'description', 'last_modified'];
  $scope.thumbs = 'thumb';
  $scope.count = 5; 
  $scope.chngcolor = function() {
    $scope.color = {'color': 'red'};
  };
 
  $scope.uploadFile = function() {

    var file = $scope.myFile;
    console.log(file); 
    if (file) {
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
          $scope.jsondata = {"thumb":"https://image.flaticon.com/icons/svg/885/885451.svg", "name":file.name, "description":"", "last_modified":$filter('date')(file.lastModified,"MM/dd/yyyy HH.mm.ss")};
          $http.post('/sample',JSON.stringify($scope.jsondata)).success(function(data,status){
            console.log("Success");
          });
          $scope.contentData = {"name":file.name, "content":evt.target.result, "last_modified":$filter('date')(file.lastModified,"MM/dd/yyyy HH.mm.ss")}
          var uploadUrl = "/fileUpload";
          $http.post(uploadUrl, JSON.stringify($scope.contentData)).success(function(data,status) {
            console.log("Success");
          });
          location.reload();
        }
    } else {
        reader.onerror = function (evt) {
        console.log("error reading file");
        }
    }
  };
}]);

app.directive('mdTable', function () {
  return {
    restrict: 'E',
    scope: { 
      headers: '=', 
      content: '=', 
      sortable: '=', 
      filters: '=',
      customClass: '=customClass',
      thumbs:'=', 
      count: '=' 
    },
    controller: function ($scope,$filter, $http, $mdDialog) {
      var orderBy = $filter('orderBy');
      $scope.tablePage = 0;
      $scope.nbOfPages = function () {
        if(!$scope.content) {return 1;}
        return Math.ceil($scope.content.length / $scope.count);
      },
      	$scope.handleSort = function (field) {
          if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
      };
      $scope.order = function(predicate, reverse) {
          $scope.content = orderBy($scope.content, predicate, reverse);
          $scope.predicate = predicate;
      };
      $scope.order($scope.sortable[0],false);
      $scope.getNumber = function (num) {
      			    return new Array(num);
      };
      /* insert displaying code here */
      $scope.display = function(name) {
        $http.get("/text", {params: {Name:name}}).then(function(response) {
            var d = response.data;
            alert(d['content']);
        });
        console.log(name);
      }

        
      var originatorEv;

      $scope.openMenu = function($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdMenu.open(ev);
      };

      $scope.edit = function(c) {
        
      };

      $scope.remove = function(c) {
        $http.get('./test.json').success(function(data) {
            console.log(data);
            name = c['name'];
            console.log(c);
            var i = 0;
            for (; i < data.length; i++) {
                if(data[i].name == name) {
                  
                  break;
                }
            }
          $http.post('/removeFS', data[i]).success(function(d, status) {
          });
          console.log(name[i]);
          data.splice(i,1);
          $http.post('/remove', data).success(function(d,status) {
          });
            location.reload();
        });
        
      }

$scope.goToPage = function (page) {
        $scope.tablePage = page;
      };
    },
    template: angular.element(document.querySelector('#md-table-template')).html()
  }
});

//UNCOMMENT BELOW TO BE ABLE TO RESIZE COLUMNS OF THE TABLE
app.directive('mdColresize', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      scope.$evalAsync(function () {
        $timeout(function(){ $(element).colResizable({
          liveDrag: true,
          fixed: true
          
        });},100);
      });
    }
  }
});

app.directive('showFocus', function($timeout) {
  return function(scope, element, attrs) {
    scope.$watch(attrs.showFocus, 
      function (newValue) { 
        $timeout(function() {
            newValue && element.focus();
        });
      },true);
  };    
});


app.filter('startFrom',function (){
  return function (input,start) {
    if (!input || !input.length) { return; }
    start = +start;
    return input.slice(start);
  }
});
