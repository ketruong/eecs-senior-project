const parser = require("./myApp/methods.js"),
      express = require('express'),
      cors = require('cors'),
      app = express(),
      data = require('./myApp/test.json'),
      fs = require('fs');
      bodyParser = require('body-parser'),
      {execSync, exec} = require('child_process');
const paths = [
    "./myApp/test.json"
];
app.use(express.static("myApp")); // myApp will be the same folder name.
app.use(cors());
app.use(bodyParser.json());

var urlencodedParser = bodyParser.urlencoded({ extended: true });

app.get('/', function (req, res,next) {
 res.redirect('/'); 
});
var initData = fs.readFileSync("./myApp/test.json");
var initJson = JSON.parse(initData);
let fileNames = new Set();
let accessDate = {};
for (var i = 0; i < initJson.length; i++) {
    fileNames.add(initJson[i].name);
    accessDate[initJson[i].name] = initJson[i].last_modified;
}
app.post('/sample',urlencodedParser,function(req,res){
   var dir = 't/';

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
   if (!fileNames.has(req.body['name'])) {
    var jsn = req.body;
    var prevData = fs.readFileSync("./myApp/test.json");
    var prevJson = JSON.parse(prevData);  
    prevJson.push(jsn);
    fs.writeFileSync("./myApp/test.json", JSON.stringify(prevJson));
    } else if(req.body['last_modified'] != accessDate[req.body['name']]) {
        var jsn = req.body;
        var prevData = fs.readFileSync("./myApp/test.json");
        var prevJson = JSON.parse(prevData);  
        for(var i = 0; i < prevJson.length; i++) {
            if(prevJson[i].name == req.body['name']) {
                prevJson.splice(i, 1);
                break;
            }
        }
        prevJson.push(jsn);
        fs.writeFileSync("./myApp/test.json", JSON.stringify(prevJson));

    }
});

app.post('/fileUpload', urlencodedParser, function(req, res) {
    var dir = 't/';

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    var tmpFile = "t/";
    tmpFile += req.body['name'];

    
    if (!fileNames.has(req.body['name'])) {
        fs.writeFileSync(tmpFile, JSON.stringify(req.body));
        fileNames.add(req.body['name']);
        
        var cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 ';
        cmd += tmpFile;
        cmd += "; rm ";
        cmd += tmpFile;
        
        var child = exec(cmd, (err, stdout, stderr) => {
            if(err) return;
            console.log(stdout);
            console.log(stderr);
        });
        console.log("Added to fs");
    
    } else if(req.body['last_modified'] != accessDate[req.body['name']]) {
        fs.writeFileSync(tmpFile, JSON.stringify(req.body));
        fileNames.add(req.body['name']);
        accessDate[req.body['name']] = req.body['last_modified'];
        var cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 ls';
        var child = execSync(cmd).toString();
        console.log(child); 
        jsn = JSON.parse(child);
        var target = ''; 
        for (var i = 0 ; i < jsn.length; i++) {
            if(tmpFile == jsn[i].name) {
            target = jsn[i];
            break;
            }
        }
        var inode = target["inode"];
        console.log(inode);
        cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 remove ';
        cmd += inode;
        console.log(cmd);
        child = execSync(cmd).toString();
        console.log(child); 
        cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 ';
        cmd += tmpFile;
        cmd += "; rm ";
        cmd += tmpFile;
        
        var child = execSync(cmd).toString();
        console.log(child);
        console.log("Added to fs");

        
    }
});

app.get('/text', function (req,res) {
    var child = execSync("rm -rf t/").toString();
    var cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 ls';
    child = execSync(cmd).toString();
    console.log(child);  
    jsn = JSON.parse(child);
    
    var target = ''; 
    var new_name = "t/" + req.query.Name;
    for (var i = 0 ; i < jsn.length; i++) {
        if(new_name == jsn[i].name) {
            target = jsn[i];
            break;
        }
    }
    
    var inode = target["inode"];
    cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 cat ';
    cmd += inode;
    
    child = execSync(cmd).toString();
    console.log(child);
    console.log(cmd);
    let rawdata = fs.readFileSync(new_name);
    res.send(JSON.parse(rawdata));
});

app.post('/remove', urlencodedParser, function(req, res) {
    fs.writeFileSync('./myApp/test.json', JSON.stringify(req.body));
});

app.post('/removeFS', urlencodedParser, function(req, res) {
    var tmpFile = "t/";
    tmpFile += req.body['name'];
    console.log(req.body);
    var cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 ls';
    var child = execSync(cmd).toString();
    console.log(child); 
    jsn = JSON.parse(child);
    var target = ''; 
    for (var i = 0 ; i < jsn.length; i++) {
      if(tmpFile == jsn[i].name) {
        target = jsn[i];
        break;
      }
    }
    var inode = target["inode"];
    console.log(inode);
    cmd = '../filesystem/bin/sfssh ../filesystem/data/image.200 200 remove ';
    cmd += inode;
    console.log(cmd);

});

app.listen(8080, function() {
    console.log('Server running at http://127.0.0.1:8080/'); 
});

