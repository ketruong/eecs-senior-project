set_property PACKAGE_PIN W21 [get_ports uart_rtl_0_rxd]
set_property IOSTANDARD LVCMOS33 [get_ports uart_rtl_0_rxd]
set_property PACKAGE_PIN Y21 [get_ports uart_rtl_0_txd]
set_property IOSTANDARD LVCMOS33 [get_ports uart_rtl_0_txd]

set_property PACKAGE_PIN AD23 [get_ports reset_n]
set_property IOSTANDARD LVCMOS33 [get_ports reset_n]

set_property IOSTANDARD LVCMOS33 [get_ports clk_50]
set_property PACKAGE_PIN AC28 [get_ports clk_50]

create_clock -period 20.00 -name clk_50 [get_ports clk_50]

set_property BITSTREAM.CONFIG.UNUSEDPIN PULLUP [current_design]