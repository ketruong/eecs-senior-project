//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
//Date        : Sun Oct  6 20:46:56 2019
//Host        : DESKTOP-LKMDMIU running 64-bit major release  (build 9200)
//Command     : generate_target Mblaze_wrapper.bd
//Design      : Mblaze_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module Mblaze_wrapper
   (clk_50,
    reset_n,
    uart_rtl_0_rxd,
    uart_rtl_0_txd);
  input clk_50;
  input reset_n;
  input uart_rtl_0_rxd;
  output uart_rtl_0_txd;

  wire clk_50;
  wire reset_n;
  wire uart_rtl_0_rxd;
  wire uart_rtl_0_txd;

  Mblaze Mblaze_i
       (.clk_50(clk_50),
        .reset_n(reset_n),
        .uart_rtl_0_rxd(uart_rtl_0_rxd),
        .uart_rtl_0_txd(uart_rtl_0_txd));
endmodule
