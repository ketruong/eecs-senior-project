# EECS Senior Project

### Building a Personal Cloud Using FPGA

Personal cloud storage is a network-attached storage device that allows users to store files onto a external server. Traditionally, users have to pay a subscription fee to host their data in the cloud e.g. Google Drive and Dropbox. Having a personal cloud storage gets rid of this royalty fee and gives users complete control of their data. The goal of this project is to create a personal cloud using a FPGA and a SATA disk. When the FPGA is connected to a personal wireless network, end-users will be able to access their data which is stored on the SATA disk.
