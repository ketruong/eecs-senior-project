#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <fnmatch.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include "utils.h"

void mkdirR(const char *dir, int last, bool* deleted, int argc, char ** argv) {
    char tmp[256];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);
    if(tmp[len - 1] == '/') tmp[len - 1] = 0;
    for(p = tmp + 1; *p; p++)
	if(*p == '/') {
	    *p = 0;
	    if(access(tmp, F_OK) == -1) {
		mkdir(tmp, 0777);
		if(access(tmp, F_OK) == -1) return; 
		checkArgs(argc, argv, tmp, deleted);
	    }
	    *p = '/';
	}
    if(last) mkdir(tmp, 0777);
}

int stringCompare( const void* a, const void* b) { 
    char const *char_a = *(char const **)a;
    char const *char_b = *(char const **)b;
    return strcmp(char_a, char_b);
}
// add slash to the end of a directory if necessary
char * addSlash(char * file){
    int lenFile = strlen(file); 
    if(file[lenFile-1] == '/'){
	char * slash = strdup(file);
	return slash;
    } else {
	char * slash = calloc(lenFile+2, sizeof(char));
	strcpy(slash, file);
	slash[lenFile] = '/';
	return slash;
    }
}

// strip file of \n and / 
char * stripFile(char * file){
    char * stripFile = strdup(file);
    int lenFile = strlen(stripFile); 
    
    // if file name ends with '\n' or '/' delete it 
    if(lenFile >= 2 && stripFile[lenFile - 1] == '\n') {
	    stripFile[lenFile - 1] = '\0';
	    if(lenFile >= 2 && stripFile[lenFile - 2] == '/') stripFile[lenFile - 2] = '\0';
    } else if(lenFile >= 2 && stripFile[lenFile - 1] == '/') 
	    stripFile[lenFile - 1] = '\0';
    return stripFile;
}

//check if a file path is a directory
int isDir(const char * path) {
   struct stat statbuf;
   if (lstat(path, &statbuf) != 0) return 0;
   return S_ISDIR(statbuf.st_mode);
}

// create relative path string
char * relPathS(char * dir, char * file) {
    int lenDir = strlen(dir);
    int lenFile = strlen(file);
    int lenRelPath = lenDir + lenFile + 2;
    char * relPath = calloc(lenRelPath, sizeof(char));
    strcpy(relPath,dir);
    if(dir[lenDir-1] != '/') strcat(relPath, "/");
    strcat(relPath, file);
    return relPath;
}

// checks if the the file in the archive is in the command line arguments 
int checkArgs(int argc, char ** argv, char * file, bool * deleted) {

    char * strippedFile = stripFile(file); 
    for (int i = 3; i < argc; i++) {
	if(!(strcmp(strippedFile,argv[i]))) {
	    free(strippedFile);
	    deleted[i-3] = 1;
	    return 1;
	}
	if(!(strncmp(strippedFile, argv[i], strlen(argv[i])))){
	    free(strippedFile);
	    deleted[i-3] = 1;
	    return 1;
	}
    }
    free(strippedFile);
    return 0;
}
// checks if the the file in the archive is in the command line arguments 
int checkArgsMod(int argc, char ** argv, char * file, bool * deleted) {
    char * strippedFile = stripFile(file); 
    int fileLen = strlen(strippedFile);
    for (int i = 3; i < argc; i++) {
	int len = strlen(argv[i]);
	if(!(strncmp(strippedFile, argv[i], len))) {
	    if(len <= fileLen - 2) if(strippedFile[len+1] == '.' && strippedFile[len] == '/')
		continue;
	    if(len <= fileLen - 3) if(strippedFile[len+2] == '.' && strippedFile[len+1] == '.' && strippedFile[len] == '/')
		continue;
	    free(strippedFile);
	    return 1;
	}
    }
    free(strippedFile);
    return 0;
}

// checks if the the file in the archive is in the command line arguments
// used only for modifying an archive 
int checkArgsX(int argc, char ** argv, char * file, bool * deleted) {

    int ret = 0;
    char * strFile = stripFile(file);
    for (int i = 3; i < argc; i++) {
	if(!(strcmp(strFile,argv[i]))) {
	    deleted[i-3] = 1;
	    ret = 1;
	    break;
        }
    } 
    free(strFile);
    return ret;
}
// copy contents from one file to another 
void contentCP(int memSize, FILE * infile, FILE * outFile) {
    unsigned int c;
	
    for (int i = 0; i < memSize; i++) {	
	c = fgetc(infile);
	fputc(c, outFile);
    }
}

// append file to the archive 
void fileAppend(int argc, char ** argv, int examined, FILE * outFile, char * file, bool * deleted, int dup) {
    
    if(!dup) { if(checkArgsX(examined, argv, file, deleted)) return; 
    } else if (checkArgs(examined, argv, file, deleted)) return; 
     
    // if(checkArgs(examined, argv, file, deleted)) return; 
    // calculate the size of the file
    struct stat st;
    if (lstat(file, &st) < 0) {
	WARN("cannot read/lstat %s", file);
	return;
    }
    
    if(S_ISLNK(st.st_mode)) return;
    
    // write the name of the file 
    fprintf(outFile, "%s\n", file);
    
    FILE * inFile;
    if(!(inFile =fopen(file, "r"))) {
	WARN("cannot read/lstat %s", file);
	return;
    }
	
    // print the size of the file
    fprintf(outFile, "%d|", (int)st.st_size);

    contentCP(st.st_size, inFile, outFile);
    fclose(inFile);
}

// recursively add a directory 
void dirAppend(int argc, char ** argv, int examined, FILE * outFile, char * dir, bool * deleted, int dup){
    DIR *dp;
    struct dirent *entry;
    struct stat buf;
	
    // remove slash if necessary
    char * sDir= stripFile(dir); 
	
    // add slash if necessary
    char * slashDir = addSlash(sDir);	
    
    int p = !checkArgs(examined, argv, slashDir, deleted);
    if(p) {
	fprintf(outFile, "%s\n", slashDir);
	fprintf(outFile, "%d|", 0);
    }
    if ((dp = opendir(dir)) == NULL) {
	if(p) WARN("cannot opendir(%s/)", dir);
	free(slashDir);
	free(sDir);
	return;
    }
    free(slashDir);
    
    // traverse contents of directory 
    while ((entry = readdir (dp))) {                  
        
        if(!strcmp(entry->d_name,".") || !strcmp(entry->d_name, "..")) continue;
        
        char * relPath = relPathS(dir,entry->d_name);
	if (lstat (relPath, &buf) < 0) WARN("lstat(%s) failed", relPath);
       
        // if directory, recurse 
        if (S_ISDIR(buf.st_mode)) dirAppend(argc, argv, examined, outFile, relPath, deleted, dup);		
       	else if (S_ISREG(buf.st_mode)) fileAppend(argc, argv, examined, outFile, relPath, deleted, dup); 
	else if(S_ISLNK(buf.st_mode)) ; 
        
        free(relPath);
    }
    free(sDir);
    closedir(dp);
}

// files do not exist in archive yet 
void append(bool * deleted, int argc, char ** argv, FILE * outFile, int dup) {
    for(int i = 3; i < argc; i++)
	if(!deleted[i-3]) {
	    struct stat st;
	    
	    // file does not exist
	    if (lstat(argv[i], &st) < 0) {
		WARN("%s does not exist",argv[i]); 
		continue;
	    }
	    deleted[i-3] = 1; 
	    if (S_ISLNK(st.st_mode)) continue;
	    else if(access(argv[i], F_OK) == -1) {
		WARN("%s does not exist",argv[i]); 
		continue;
	    } else if (S_ISDIR(st.st_mode)) dirAppend(argc, argv, i, outFile, argv[i], deleted, dup); 
	    else fileAppend(argc, argv, i, outFile, argv[i], deleted, dup); 
        }
}


// easily copy contents of command line to new archive
void newArchive(int argc, char ** argv, char* ark, int std, int dup) {
		
    // open file and write 
    FILE * outFile;
    char * archive = ark;
    bool * deleted = calloc(argc - 3, sizeof(bool));
    if (!std) {
	if(!(outFile = fopen(archive, "w"))) WARN("cannot open %s", archive);
	else append(deleted, argc, argv, outFile, dup); 
	free(deleted);
	if(outFile) fclose(outFile);
    } else append(deleted, argc, argv, stdout, dup); 
}

// transfer contents in specified format
void transfer(FILE * outFile, int memSize, char * memName, FILE * infile, char* newLine) {
    // transfer name and size
    fprintf(outFile, "%s", memName);
    fprintf(outFile, "%s", newLine);
    fprintf(outFile, "%d|", memSize);

    //transfer file contents
    contentCP(memSize,infile, outFile);    
}

// checks to see if everything is deleted
void delErr(int argc, char ** argv, bool * deleted){
    for(int i = 3; i < argc ; i++) 
	if(!deleted[i-3]) WARN("cannot find %s", argv[i]);
}
void makeTemp(char tmpFile[], FILE * outFile) {
    int newFile = mkstemp(tmpFile);

    // check if there was error creating temp file
    if (newFile == -1) DIE("Could not make temp file %s", tmpFile); 

    // prepare to write to file
    unlink(tmpFile);
}
// if the archive exists, modify first and then add
void modArchive(int argc, char** argv,  char* archive, int std, int dup) {
    // file/directory name 
    char memName[PATH_MAX];

    // the size of the file/directory 
    int memSize = 0;
    // for standard input
    if(!std && isDir(archive)) {
	WARN("cannot modify %s", archive);
	return;
    }
    bool * deleted = calloc(argc - 3, sizeof(bool));
    char **  found = calloc(argc, sizeof(char *));
    int size = argc;
    int index = 0;
    FILE * infile;
    if (std) infile = stdin;
    else if(!(infile = fopen(archive, "r"))) DIE("%s does not exist", archive); 

    // output file  
    FILE * outFile;
    int rewrite = 0;
    char tmpFile[] = "ARCHIVE.BAK";
    //makeTemp(tmpFile, outFile); 
    if (std) outFile = stdout;
    else if(!(outFile = fopen(tmpFile, "w"))) DIE("%s does not exist", tmpFile); 
    // get name of file/directory in archive
    while(fgets(memName, PATH_MAX, infile)) {
	rewrite = 1;
	// get the size of the file/directory
	fscanf(infile, "%d", &memSize);
	if (strchr(memName, '|') || fgetc(infile) != '|') {
	    if(infile) fclose(infile);
	    if(outFile) {
		fclose(outFile);
		remove(tmpFile);
	    }
	    free(deleted);
	    free(found);
	    DIE("cannot open archive %s", archive);
	}
	// printf("%s", memName); 
	if(checkArgsMod(argc, argv, memName, deleted)) {
	    // copy updated text over	
	    char * strFile = stripFile(memName);	
	    FILE * mod = NULL;
	    struct stat buf;
	    if(lstat(strFile, &buf) < 0) {
	    	transfer(outFile, memSize, memName, infile, "");
		free(strFile);
		continue;
	    } else if(S_ISDIR(buf.st_mode)) {
		transfer(outFile, 0, memName, mod, "");
		if(memName[strlen(memName)-2] != '/') rewrite = 0; 
		if(access(strFile, F_OK) == -1 ) {
		    for (int i = 3; i < argc; i++) {
			if(!(strcmp(strFile,argv[i]))) {
			    deleted[i-3] = 1;
			}
		    } 
		}
	    } else if (S_ISLNK(buf.st_mode) || !(mod = fopen(strFile, "r"))) {
		transfer(outFile, memSize, memName, infile, "");
		free(strFile);
		continue;
	    } else {
		if(memName[strlen(memName)-2] == '/') rewrite = 0; 
		transfer(outFile, (int)buf.st_size, memName, mod, "");
	    }
	
	    if(index == size - 1) {
		size*=2;
		found = realloc(found, sizeof(char *) * size);
	    }
	    if(rewrite) {
		found[index] = strdup(strFile); 
		index++;
	    }
	    for (int i = 0; i < memSize; i++) getc(infile);
	    if(mod) fclose(mod);
	    free(strFile); 
	    continue;
	}
	transfer(outFile, memSize, memName, infile, "");
    }
    char ** new_argv = calloc((argc+index), sizeof(char *));
    new_argv[0] = new_argv[1] = new_argv[2] = "";
    bool * new_deleted = calloc((argc+index-3), sizeof(bool));

    for (int k = 0; k < argc+index-3; k++) {
	if(k >= index) new_deleted[k] = deleted[k-index]; 
	else new_deleted[k] = 1;
	if(k < argc - 3) new_argv[k+3+index] = strdup(argv[k+3]);
	if(k < index) new_argv[k+3] = strdup(found[k]);
    }
    // for(int j = 3; j < argc + index ; j++) printf("%s\n", new_argv[j]);
    if(!std && infile) fclose(infile);
    if (!std) {
	rename(tmpFile, archive);
    }
    append(new_deleted, argc+index, new_argv, outFile, dup); 
    for (int k = 3; k < argc+index; k++) {
	free(new_argv[k]);
	if (k-3 < index) free(found[k-3]);
    }
    free(new_argv);
    free(new_deleted);
    if(outFile) fclose(outFile);
    free(found);
    free(deleted);
}
