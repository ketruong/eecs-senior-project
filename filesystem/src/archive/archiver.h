#ifndef ARCHIVER_H
#define ARCHIVER_H
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <limits.h>
void r(int argc, char ** argv);
void t(char * archive, char ** argv, int argc);
void d(int argc, char ** argv);
void x();
#endif
