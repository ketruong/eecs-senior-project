#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <fnmatch.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>

// Write message to stderr using format FORMAT
#define WARN(format,...) fprintf (stderr, "farthing: " format "\n", __VA_ARGS__)

// Write message to stderr using format FORMAT and exit.
#define DIE(format,...)  WARN(format,__VA_ARGS__), exit (EXIT_FAILURE)

// makes directories recursively for X
void mkdirR(const char *dir, int last, bool* deleted, int argc, char ** argv); 

// compare strings for sorting 
int stringCompare(const void* a, const void* b); 

// add forward slash to the end of the file name
char * addSlash(char * file);

// strip \n and forward slashes to file name 
char * stripFile(char * file);

// check if a file is a directory 
int isDir(const char * path); 

// add relative path names
char * relPathS(char * dir, char * file);

// check if it's in the command line for R
int checkArgs(int argc, char ** argv, char * file, bool * deleted); 

// check if it's in the command line for modifying archive 
int checkArgsMod(int argc, char ** argv, char * file, bool * deleted); 

// check if it's in the command line for extract 
int checkArgsX(int argc, char ** argv, char * file, bool * deleted);

// copy contents from one file to another
void contentCP(int memSize, FILE * infile, FILE * outFile); 

// append files 
void fileAppend(int argc, char ** argv, int examined, FILE * outFile, char * file, bool * deleted, int dup); 

// append directories 
void dirAppend(int argc, char ** argv, int examined, FILE * outFile, char * dir, bool * deleted, int dup); 

// append directories and files  
void append(bool * deleted, int argc, char ** argv, FILE * outFile, int dup); 

// make a new archive 
void newArchive(int argc, char ** argv, char* ark, int std, int dup); 

// transfer from one file to another 
void transfer(FILE * outFile, int memSize, char * memName, FILE * infile, char * newLine); 

// print out error messages
void delErr(int argc, char ** argv, bool * deleted);

// modify an archive 
void modArchive(int argc, char** argv,  char* archive, int std, int dup); 
