#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <fnmatch.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include "utils.h"
#include "archiver.h"

void r(int argc, char ** argv) {
    int std = 0;
    if(!strcmp(argv[2],"-")) {
	std = 1;
	modArchive(argc, argv, "", std, 0);
    } else {
	// new file 
	if (access(argv[2], F_OK) == -1) newArchive(argc, argv, argv[2], std, 1); 
	else modArchive(argc, argv, argv[2], std, 0);
    }
}

void t(char * archive, char ** argv, int argc) {
 
    FILE * fp; 
    // file/directory name 
    char memName[PATH_MAX];

    // the size of the file/directory 
    int memSize = 0;
    int a =!strcmp(archive,"-");
   		
    if(isDir(archive)) return; 
    
    // check if file exists 
    if (a) fp = stdin;
    else if (!(fp = fopen(archive, "r"))) DIE("cannot open archive %s", archive);

    bool * deleted = calloc(argc - 3, sizeof(bool));
    char **  found = calloc(argc, sizeof(char *));
    int size = argc;
    int index = 0;
    
    // get name of file/directory 
    while(fgets(memName, PATH_MAX, fp)) {
	char * strFile = stripFile(memName);
	// get the size of the file/directory  
	fscanf(fp, "%d", &memSize);

	// corrupted archive file 
	if(fgetc(fp) != '|') {
	    if(fp) fclose(fp);
	    free(found);
	    free(deleted);
	    free(strFile);
	    DIE("cannot open archive %s", archive);
	}
 
	// print 
	if(argc == 3 || checkArgs(argc, argv, memName, deleted)) {
	    printf("%8d %s", memSize, memName);
	}
	
	if(index == size - 1) {
	    size*=4;
	    found = realloc(found, sizeof(char *) * size);
	}

	// add to the array of things that have been seen
	found[index] = strdup(strFile); 
	index++;
 
	// skip past content of the file/directory 
	for (int i = 0; i < memSize; i++) fgetc(fp);
	free(strFile);
    }
    // figure out what hasn't been seen yet
    for(int i = 3; i < argc ; i++) {
	if(!deleted[i-3]) {
	    int print = 1;
	    for (int j = 0; j < index; j++) {
		if(!strncmp(argv[i],found[j],strlen(argv[i]))) {
		    print = 0;
		    break;
		}
	    }
	    if(print) WARN("cannot find %s", argv[i]);
	}
    }
    // free everything
    for(int k = 0; k < index; k++) free(found[k]);
    free(found);  
    free(deleted);
    if(!a && fp) fclose(fp);
}

void d(int argc, char ** argv) {
    FILE * infile;
    char * archive = argv[2];
     // file/directory name 
    char memName[PATH_MAX];

    // the size of the file/directory 
    int memSize;
    
    int size = argc;
    int index = 0;
    int a = !strcmp(archive, "-");
    
    // check if file exists
    if(a) infile = stdin;
    else if (!(infile = fopen(archive, "r"))) DIE("cannot open archive %s", archive);
    
    //create temp file to copy content over 
    char tmpFile[] = "ARCHIVE.BAK";
    FILE * outFile;
    if(!a) {
	if(!(outFile = fopen(tmpFile, "w"))) DIE("cannot open archive %s", archive);
    } else outFile = stdout;

    bool * deleted = calloc(argc - 3, sizeof(bool));
    char **  found = calloc(argc, sizeof(char *));
    // get name of file/directory in archive 
    while(fgets(memName, PATH_MAX, infile)) {

        // get the size of the file/directory
        fscanf(infile, "%d", &memSize);
	
	// error 
	if(fgetc(infile) != '|') {
	    free(found);
	    free(deleted);
	    if(infile) fclose(infile);
	    if(outFile) fclose(outFile);
	    remove(tmpFile);
	    DIE("cannot open archive %s", archive);
	}
        
        // skip over files that are being deleted
        if(checkArgs(argc, argv, memName, deleted)) {
	    for (int i = 0; i < memSize; i++) getc(infile);
            continue;
        }
	
	if(index == size - 1) {
	    size*=4;
	    found = realloc(found, sizeof(char *) * size);
	}

	// add to things seen 
	found[index] = strdup(memName); 
	index++;

	transfer(outFile, memSize, memName, infile, "");
    }
    // rename file;
    if(!a) {
	rename(tmpFile, archive);
	if(outFile) fclose(outFile);
	if(infile) fclose(infile);
    }

    // print errors that have not been seen 
    for(int i = 3; i < argc ; i++) {
	if(!deleted[i-3]) {
	    int print = 1;
	    for (int j = 0; j < index; j++) {
		if(!strncmp(argv[i],found[j],strlen(argv[i]))) {
		    print = 0;
		    break;
		}
	    }
	    if(print) WARN("cannot find %s", argv[i]);
	}
    }
    for(int k = 0; k < index; k++) free(found[k]);
    free(found);
    free(deleted);
}
void x(int argc, char ** argv){
    FILE * infile;
    char * archive = argv[2];
    
    // file/directory name 
    char memName[PATH_MAX];

    // the size of the file/directory 
    int memSize = 0;
    
    if(!strcmp(archive,"-")) infile = stdin; 
    else if (!(infile = fopen(archive, "r"))) DIE("cannot open archive %s", archive);
   
    bool * deleted = calloc(argc - 3, sizeof(bool));
    char **  found = calloc(argc, sizeof(char *));
    int size = argc;
    int index = 0;
    
    // get name of file/directory in archive 
    while(fgets(memName, PATH_MAX, infile)) {

        // get the size of the file/directory
        fscanf(infile, "%d", &memSize);
        if(fgetc(infile) != '|') {
	    free(found);
	    free(deleted);
	    if(infile) fclose(infile);
	    DIE("cannot open archive %s", archive);
	}
	char * strFile = stripFile(memName);
        if(argc == 3 || checkArgs(argc, argv, strFile, deleted) || checkArgsX(argc, argv, strFile, deleted)) {
	    //create temp file to copy old content over 
	    int len = strlen(memName);
	    if(memName[len-2] =='/' ) {
	    
		struct stat sb;
		lstat(memName, &sb);
		// check if directory already exists
		if (isDir(strFile));
		
		// it's a file in the local
		else if (access(strFile, F_OK) != -1) {
		    WARN("cannot create %s/", strFile); 
		    for (int i = 3; i < argc; i++) {
			if(!(strcmp(strFile,argv[i]))){
			    deleted[i-3] = 0;
			    break;
			}
		    } 
		    free(strFile);
		    continue;
		} else {
		    mkdirR(strFile, 1, deleted, argc, argv);
		    
		    // did not make directory correctly
		    if(!isDir(strFile)) {
			WARN("cannot create %s/", strFile); 
			for (int i = 3; i < argc; i++) {
			    if(!(strncmp(strFile,argv[i], strlen(argv[i])))) {
				deleted[i-3] = 0;
				break;
			    }
			}
		    	free(strFile);	
			continue;
		    }
		}
		if(index == size - 1) {
		    size*=4;
		    found = realloc(found, sizeof(char *) * size);
		}
		found[index] = strdup(strFile); 
		index++;
		free(strFile);	
		continue;
	    }
	    mkdirR(strFile, 0, deleted, argc, argv);
	    FILE * outFile;
        char nstrFile[128] = "~/Dropbox/Yale/EE/eecs_senior_project/code/website/";
        strcat(nstrFile,strFile);
	    if (!(outFile= fopen(strFile, "w"))) { 
		for (int i = 0; i < memSize; i++) fgetc(infile);
		// printf("%s\n", strFile);
		WARN("cannot create %s", nstrFile);
		for (int i = 3; i < argc; i++) {
			if(!(strncmp(strFile,argv[i], strlen(argv[i])))) {

			    deleted[i-3] = 0;
			    break;
			}
		    }
		continue;
	    }
    
	    // print the size of the file
	    contentCP(memSize, infile, outFile);
	    if(index == size - 1) {
		size*=2;
		found = realloc(found, sizeof(char *) * size);
	    }
	    found[index] = strdup(strFile); 
	    index++;
	    free(strFile);
	    if(outFile) fclose(outFile);
	    continue;
        }
	free(strFile);
	for (int i = 0; i < memSize; i++) getc(infile);
    }

    // check for things that have not been seen 
    for(int i = 3; i < argc ; i++) {
	if(!deleted[i-3]) {
	    int print = 1;
	    for (int j = 0; j < index; j++) {
		if(!strncmp(argv[i],found[j],strlen(argv[i]))) {
		    print = 0;
		    break;
		}
	    }
	    if(print) WARN("cannot find %s", argv[i]);
	}
    }
    
    // free everything
    if(infile) fclose(infile);
    for(int k = 0; k < index; k++) free(found[k]);
    free(found);
    free(deleted);
}

int main (int argc, char ** argv) {
    // check number of arguments 
    if (argc < 3) DIE("%s", "farthing r|x|d|t archive [filename]");
	    
    // check if the command is more than one character
    if (strlen(argv[1]) != 1) DIE("%s", "farthing r|x|d|t archive [filename]");
    char * names[argc-3];
    for (int i = 3; i < argc; i++) {
	int len = strlen(argv[i])-1;
	for (int j = len; j>=0; j--) {
	    if (argv[i][j] == '/') argv[i][j] = '\0';
	    else break;
	}
	names[i-3] = (argv[i]);
    }

    qsort(names, argc-3, sizeof(char*), stringCompare);
    for (int i = 3; i < argc; i++) argv[i] = names[argc-i-1];
    // handle cases
    switch(argv[1][0]) {
        case 'x':
            x(argc, argv);
            break;
        case 'd':
            d(argc, argv);
            break;
        case 't':
            t(argv[2], argv, argc);
            break;
	case 'r':
	    r(argc, argv);
	    break;
        default:
	    DIE("%s", "farthing r|x|d|t archive [filename]");
    } 
    return EXIT_SUCCESS;
}
