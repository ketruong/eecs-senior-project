#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Decode.h"
#include "Encode.h"

#define MAX_BITS 12

int main(int argc, char** argv) {
   
    // Turn off buffering (and thereby the allocation of buffers)
    setvbuf (stdin,  NULL, _IONBF, 0);    // Avoid malloc()ing buffers
    setvbuf (stdout, NULL, _IONBF, 0);    // for stdin/stdout
    
    // flags for maxbits and ratio 
    bool r = 0, mb = 0, mr = 0;
    int maxbits = MAX_BITS; 
    double ratio = 0;
   
    if(!(argc %2)) DIE("usage: %s", "encode/decode");


    // decode
    if(!(strcmp(argv[0], "/Users/Truong/Dropbox/Yale/EE/senior_project/code/filesystem/bin/decode"))) {
         
        // too many command line arguments for decode
        if(argc > 1) DIE("usage: %s", "decode");
        
        codeTable * table = decode();
        destroyTable(table);
    // encode 
    } else if(!strcmp(argv[0], "/Users/Truong/Dropbox/Yale/EE/senior_project/code/filesystem/bin/encode")) {
        // parsing for -m and -p flag
        for (int i = 1; i < argc; i++) {
            
            if(!r && !mb && !strcmp("-m", argv[i])) {
                mb = 1;

            //maxbit flag is set
            } else if(mb) {
                int inputMaxBit = atoi(argv[i]);
                
                // -m MAXBITS has 0 or some non-numerical input 
                if (!inputMaxBit) maxbits = MAX_BITS; 
                
                // if valid input, change the maxbits
                if (inputMaxBit > 8 && inputMaxBit < 22) maxbits = inputMaxBit;

		if (inputMaxBit <=8 || inputMaxBit >= 22) maxbits = 12;
                
                mb = 0;
            
            // ratio flag is set
            } else if(!mb && !r && !strcmp("-r", argv[i])) {
                r = 1;
		mr = 1;
	    
	    } else if(r) {
		ratio = atof(argv[i]);
		r = 0;
            // incorrect flag
            } else DIE("encode: %s", "invalid input");
            
        }

	if (mb || r)  DIE("encode: %s", "invalid input!!!!");
	if (mr && ratio <= 0)  DIE("encode: %s", "invalid input!");
        
	codeTable * table = encode(maxbits, mr, ratio);
        destroyTable(table);
    // error 
    } else DIE("encode/decode: %s %s", "illegal command name", argv[0]);
    
    exit(0);
}
