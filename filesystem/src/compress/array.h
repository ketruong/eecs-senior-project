#ifndef ARRAY_H 
#define ARRAY_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include "code.h"
// Error Printing 
#define WARN(format,...) fprintf (stderr, "" format "\n", __VA_ARGS__)
#define DIE(format,...)  WARN(format,__VA_ARGS__), exit (EXIT_FAILURE)


// hashing function
#define INIT(p,c) (p)                               // Initial slot
#define STEP(p,c) (2 * (((p)<<CHAR_BIT)|(c)) + 1)   // Step to next slot
#define REDUCE(h,s) ((h) & ((s)-1))                 // Reduce to [0,SIZE-1)


// Empty, Reset, Increase, and End
#define EMP 0
#define R 1
#define INC 2
#define END 3

#define SPECIAL 4

// Starting number of bits 
#define START 9

// how many entries are in the table 
extern int entries; 

// size of the table
extern int size; 

// entry in the table 
typedef struct tab {
    unsigned int pref: 22;
    unsigned int used: 1;
    unsigned int c: 8;
} tab;

// code Table is represented as a hashTable and as an array
typedef struct codeTable {
    tab * hashTable;
    unsigned int curBit: 6;
    unsigned int maxBit: 6;
    unsigned int entries: 22;
    unsigned int size : 22;
} codeTable;

// make the table bigger
void resizeTable(codeTable * table, unsigned int * pref);

// get curBits used
unsigned int getcurBit(codeTable * table);

// get maxBits
unsigned int getmaxBit(codeTable * table);

// check if the table is full 
bool isFull(codeTable * table);

// creates the table with 2^maxbits entries 
// allocates space for an array and hash table 
codeTable * createTable(int maxbits, int d);

// searches the hash table to see if the pref and char is already inserted 
int searchCode(codeTable * table, unsigned int pref, int c);

// inserts the pref and char into the hash table and array
void insertTable(codeTable * table, unsigned int pref, int c);

// set table based on encode 
void reset(codeTable * table);

// for decode: recursively prints out the characters instead of using stack 
unsigned int printDec(codeTable * table, int c); 

// frees all the memory
void destroyTable(codeTable * table);

#endif
