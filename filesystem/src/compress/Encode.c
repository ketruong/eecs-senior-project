#include "Encode.h"

// starting number of bits is 9
unsigned int bits = START;

// if the number of entries is greater than 2^bits + 1, add another bit 
// signal that the number of bits has increased
void add(codeTable * table, unsigned int pref, unsigned int c, int r , int maxbits) {
    if (isFull(table) && bits <  maxbits) {
	putBits(bits, INC);
	bits++;
    } else if (isFull(table) && bits <  maxbits) return;
    insertTable(table, pref, c);
}
codeTable * encode(int maxbits, int r, double ratio) {
    putBits(6, maxbits);
    // create table
    codeTable * table = createTable(maxbits, 0);
    // code is empty at the beginning 
    unsigned int code = EMP;
    // input character
    unsigned int c; 
    // send number of bits and escape bit 
    
    int block = (int) ratio;
    float rat = ratio - block;
    unsigned long sent = 0;
    unsigned long read = 0;
    unsigned long codes = 0;
    int searched;
    // loop through input 
    while((c = getchar()) != EOF){
		
        // (PREF, Char) is in the hashTable
	searched = searchCode(table,code,c);
        if(searched>=0) code = searched; 
        
	// not in the hashTable 
        else {
            // output the code
	    putBits(bits, code);
	    sent += bits;
            
            // add (CODE, CHAR) to the table 
            add(table, code, c, r, maxbits); 
                
            /* code associated with (EMP, CHAR) pair */
            code = searchCode(table,EMP,c);

	    // counts number of codes for reset 
	    codes++;
        }

	read += CHAR_BIT;

	if(codes >= block && r && (sent > rat * read)) {
	    putBits(bits, R);
	    reset(table);
	    sent = bits;
	    bits = START;
	    codes = 1;
            code = searchCode(table,EMP,c);
	}

	if (codes >= block) {
	    codes = 0;
	    read = 0;
	    sent = 0;
	}

    }
    
    // print last code 
    if(code != EMP) putBits(bits, code);
    
    flushBits();
   
    return table;
}
