#ifndef ENCODE_H
#define ENCODE_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "array.h"

extern unsigned int bits; 

codeTable * encode(int maxbits, int r, double ratio);
#endif
