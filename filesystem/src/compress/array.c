#include "array.h"


// check if the codeTable is full 
bool isFull(codeTable * table) {
    return (table->entries >  0.99 *  table->size) ; 
}

// creates table of size of 2^ MAXBITS with an array and hashTable 
codeTable * createTable(int maxbits, int d) {
     
    int i;
    codeTable * table = calloc(1, sizeof(codeTable));

    // set the current bits by the table 
    table->curBit = START;

    // set the maximum number of bits the table can use
    table->maxBit = maxbits;

    // set the size of the table 
    table->size = 1 << START;
    
    // make space for the hashTable
    table->hashTable = malloc(table->size *  sizeof(tab)); 
    
    // initialize entries 
    for(i = SPECIAL; i < table->size; i++) table->hashTable[i]= (tab) {0,0,0};
    
    // reserve for INC, END, and R
    for(i = 0; i < SPECIAL; i++) table->hashTable[i] = (tab) {(1<< 22) -1, 1, i}; 
    
    // initialize all one letter strings 
    for(i = 0; i < 256; i++) insertTable(table, 0, i);
     
    return table;
}
int addTable(tab * table, unsigned int pref, int c, int offset) {
    //get index from first hash
    unsigned int in = INIT(pref,c), st = STEP(pref,c);
    unsigned int index = REDUCE(in, offset); 
    //if collision occurs
    if (table[index].used || index == pref) {
            
        // loop until there is an empty spot;
        while (true) {
            // get newIndex
            in += st; 
            index = REDUCE(in, offset); 
            // if no collision occurs, store the key
            if (!table[index].used && (pref != index)){
                break;
            }
        }
    }
    return index;
}

int resetCode(tab * hashTable, tab * new_array, unsigned int code, unsigned long offset) {
    
    if(!hashTable[code].used) return code;	
    if(hashTable[code].pref == EMP) { 
	unsigned long index = addTable(new_array, EMP, hashTable[code].c, offset);
	new_array[index] = (tab) {EMP, 1, hashTable[code].c};
	hashTable[code].pref = index;
	hashTable[code].used = 0;
	return index;
    }
    if(hashTable[hashTable[code].pref].used) 
	resetCode(hashTable, new_array, hashTable[code].pref, offset);	
    
    unsigned long index = addTable(new_array, hashTable[hashTable[code].pref].pref, hashTable[code].c, offset);
    new_array[index] = (tab) {hashTable[hashTable[code].pref].pref, 1, hashTable[code].c};
    hashTable[code].pref = index;
    hashTable[code].used = 0;
    return index;

}

void resizeTable(codeTable * table, unsigned int * pref) {
    int i;
    // increase the number of bits used in table 
    table->curBit = table->curBit + 1;

    unsigned long offset = table->size * 2;

    // make space for new array
    tab * new_array =  malloc(offset *  sizeof(tab));

    // initalize new array
    for(i = 0; i < offset; i++) new_array[i] = (tab) {0,0,0};

    // reserve for INC, END, and R
    for(i = 0; i < SPECIAL; i++) new_array[i] = (tab) {(1 << 22) -1, 1, i}; 

    for (i = SPECIAL; i < table->size; i++) {
	if(table->hashTable[i].used && i != *pref) {
	    resetCode(table->hashTable, new_array, i, offset);
	}

	if(table->hashTable[i].used && i == *pref) {
	    *pref = resetCode(table->hashTable, new_array, i, offset);
	}
    }
        
    free(table->hashTable);
    // update hashTable
    table->hashTable = new_array;
    table->size = offset;

}
// for encode: insert into hashTable using double hashing 
void insertTable(codeTable * table, unsigned int pref, int c){
    
    // if is full
    if(isFull(table)) {
	if (table->curBit >= table->maxBit) return;
	resizeTable(table, &pref);
    }
         
    int offset = table->size; 
    int index = addTable(table->hashTable, pref, c, offset);
    // add entry into hashTable 
    table->hashTable[index] = (tab) {pref,1, c}; 
    table->entries++;
}

// searches the hash table to see if the pref and char is already inserted 
int searchCode(codeTable * table, unsigned int pref, int c) {
    unsigned int in = INIT(pref,c), st = STEP(pref,c);
    unsigned int index = REDUCE(in, table->size); 
    unsigned int start = index;
    
    // loop until the right triple is found else return -1 to indicate not found
    while(table->hashTable[index].used) {
        if(table->hashTable[index].pref == pref && table->hashTable[index].c == c) {
	    return index; 
	}
        in += st;
        index = REDUCE(in, table->size); 
        if(index == start) return -1;
    }
    return -1;
}


// resets table if ratio is over 
void reset(codeTable * table) {

    table->size = 1 << START;
    table->entries = 0;
    table->hashTable = calloc(table->size, sizeof(tab));
    int i; 
    // initialize entries 
    for(i = SPECIAL; i < table->size; i++) table->hashTable[i]= (tab) {0,0,0};
    
    // reserve for INC, END, and R
    for(i = 0; i < SPECIAL; i++) table->hashTable[i] = (tab) {(1<< 22) -1, 1, i}; 
    
    // initialize all one letter strings 
    for(i = 0; i < 256; i++) insertTable(table, 0, i);

}

// for decode: recursively prints out the characters instead of using stack 
unsigned int printDec(codeTable * table, int code) {

    unsigned int K = table->hashTable[code].pref == EMP ? table->hashTable[code].c : printDec(table, table->hashTable[code].pref);
    
    // print character
    table->hashTable[code].pref == EMP ? putBits(CHAR_BIT,K) : putBits(CHAR_BIT,table->hashTable[code].c); 

    return K;
}

void destroyTable(codeTable * table) {
}
