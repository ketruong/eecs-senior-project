#include "Decode.h"

// starting number of dbits 
unsigned int dbits = START;

// safely increase the number of dbits to read
void increment(int maxbits, unsigned int * oldCode, codeTable * table) {
    if((dbits +1) <= maxbits) {
	dbits++;
	resizeTable(table, oldCode);
    } else return;
}

// reset table 
void ratioTable(codeTable * table, unsigned int * oldC){
    reset(table);
    *oldC = EMP;
    dbits = START;
}

codeTable * decode() {

    // get maxbits and the ratio flag
    unsigned int maxbits = getBits(6);
    // Something went wrong with encode
    if (maxbits <= 8 || maxbits > 22) DIE("Decode: %s","corrupted file!!!"); 
    
    // create the table
    codeTable * table = createTable(maxbits, 1);
    
    // KwKwK problem flag;
    int kwkwk = 0;
    
    unsigned int * oldCode = calloc(1,sizeof(int)); 
    unsigned int * newCode = calloc(1,sizeof(int)); 
    unsigned int * Code = calloc(1,sizeof(int)); 
    
    // initially empty 
    *oldCode = EMP;
    
    int found; 
    
    // for getting characters from the stream
    unsigned int c, K; 
    
    while((c = getBits(dbits)) != EOF) {

        *newCode = *Code = c;
	        // identify if flags have occurred
        switch(*Code) {
	    case INC:
		increment(maxbits, oldCode, table);
		continue;
	    case R:
                ratioTable(table, oldCode);
                continue;
            case END:
                goto exit_loop;
            default:
                break;
        }
	found = table->hashTable[*Code].used ? 0:1; 
       
	// kwkwk event 
	kwkwk = found ? 1 : 0;

	// update code based on kwkwk event 
	*Code = found ? *oldCode: *Code; 
        
        // recursively print using the Code 
        K = printDec(table, *Code);         
        
        // print last letter because of KwKwK
        if (kwkwk) putBits(CHAR_BIT,K);
        
        // add to the table if possible
        if (*oldCode != EMP) insertTable(table,*oldCode, K);
        
        *oldCode = *newCode;
    }

    // received END code
    exit_loop: ;

    // free everything 
    free(oldCode);
    free(newCode);
    free(Code);
    return table; 
}
