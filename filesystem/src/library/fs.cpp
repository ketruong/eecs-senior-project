// fs.cpp: File System

#include "sfs/fs.h"

#include <algorithm>

#include <assert.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h>

// Load Inode based on inumber  -----------------------------------------------

// bool load_inode(size_t inumber, Inode *node);

// Save Inode based on inumber  -----------------------------------------------

// bool save_inode(size_t inumber, Inode *node); 
    

// Debug file system -----------------------------------------------------------

void FileSystem::debug(Disk *disk) {
    Block block;

    // Read Superblock
    disk->read(0, block.Data);

    printf("SuperBlock:\n");
    printf("    %u blocks\n"         , block.Super.Blocks);
    printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
    printf("    %u inodes\n"         , block.Super.Inodes);

    // Read Inode blocks
    size_t inode_blocks = block.Super.InodeBlocks + 1;
    for (size_t i = 1; i < inode_blocks ; i++) {
	disk->read(i, block.Data);

	for (size_t j = 0; j < INODES_PER_BLOCK; j++) {
	    Inode x = block.Inodes[j];
	    if (x.Valid) {
		printf("Inode %li \n", (i-1) * INODES_PER_BLOCK + j);
		printf("    size: %u bytes\n"	    , x.Size);
		printf("    name: %s\n"		    , x.Name);
		int dBlock = 0;
		for (unsigned int k = 0; k < POINTERS_PER_INODE; k++) { 
		    if (x.Direct[k]) { 
			dBlock++;
		    }
		}
		
		printf("    direct block: %u\n"	    , dBlock); 
	    }
	}
    }
}
void FileSystem::ls(Disk * disk) {
    Block block;
    
    disk->read(0, block.Data);

    // Read Inode blocks
    size_t inode_blocks = block.Super.InodeBlocks + 1;
    std::string JSON = "[";
    for (size_t i = 1; i < inode_blocks ; i++) {
        disk->read(i, block.Data);
        for (size_t j = 0; j < INODES_PER_BLOCK; j++) {
            Inode x = block.Inodes[j];
            if (x.Valid && x.Name) {
                JSON= JSON +"{\"name\":\"";
                for (int z = 0; x.Name[z] != '\0' ; z++) JSON+= x.Name[z];

                //JSON= JSON + newStr;
                JSON= JSON + "\",\"inode\":";
                JSON= JSON +  std::to_string((i-1) * INODES_PER_BLOCK + j);
                JSON= JSON +"},";
            }
        }
    }
    if(JSON.length() > 1)
        JSON = JSON.substr(0, JSON.length() -1);
    printf("%s]", JSON.c_str());

    return;
}
// Format file system ----------------------------------------------------------

bool FileSystem::format(Disk *disk) {
    // check if disk is mounted 
    if (!disk || disk->mounted()) return 0;

    // Write superblock
    Block block;
    memset(block.Data, 0 , Disk::BLOCK_SIZE);
    block.Super.Blocks = disk->size();
    block.Super.InodeBlocks= ceil(block.Super.Blocks/10.0);
    block.Super.Inodes= block.Super.InodeBlocks * INODES_PER_BLOCK;

    disk->write(0, block.Data);

    Block dummyBlock;
    memset(dummyBlock.Data, 0, Disk::BLOCK_SIZE);
    for (unsigned int k = block.Super.InodeBlocks+1; k < block.Super.Blocks; k++) {
	disk->write(k, dummyBlock.Data);	
    }
     
    Block iBlock;
    memset(iBlock.Data, 0, Disk::BLOCK_SIZE); 
    for (unsigned int i = 1; i < block.Super.InodeBlocks + 1; i++ ) {
	
	for (unsigned int j = 0; j < INODES_PER_BLOCK; j++) {
	    iBlock.Inodes[j].Valid = 0;
	    
	}
	disk->write(i, iBlock.Data);

    }
    return true;
}

// Mount file system -----------------------------------------------------------

bool FileSystem::mount(Disk *disk) {
    if (!disk) return 0;

    // Read superblock
    Block block;
    disk->read(0, block.Data);
    unsigned int inodes  = block.Super.InodeBlocks; 
    unsigned int size	 = block.Super.Blocks;
    // Set device and mount
    disk->mount();
    this-> d = disk; 
    this->inode_blocks = inodes; 
    // Allocate free block bitmap
    std::vector<bool> newDMap(size -inodes - 1, 0);
    this->d_bitmap = newDMap;
    for (unsigned int l = 0; l < size - inodes - 1; l++) {
	this->d_bitmap[l] = 0;
    }
    std::vector<bool> newIMap(inodes * INODES_PER_BLOCK , 0);
    this->i_bitmap = newIMap;

    for (unsigned int j = 1; j < inodes + 1; j++ ) {
	disk->read(j, block.Data);
	for (unsigned int k = 0; k < INODES_PER_BLOCK; k++) {
	    this->i_bitmap[(j-1) * INODES_PER_BLOCK + k] = block.Inodes[k].Valid;
	    if(block.Inodes[k].Valid) {
		for (unsigned int i = 0; i < POINTERS_PER_INODE; i++ ) {
	    
		    if (block.Inodes[k].Direct[i]) {
			//printf("%u!\n", block.Inodes[k].Direct[i]);
			this->d_bitmap[block.Inodes[k].Direct[i] - (inodes + 1)] = 1;
		    }
		}
		if(block.Inodes[k].Indirect) {
		    this->d_bitmap[block.Inodes[k].Indirect - (inodes + 1)] = 1;
		    //printf("%u@\n", block.Inodes[k].Indirect );
		    Block ind;
		    d->read(block.Inodes[k].Indirect, ind.Data);
		    for (unsigned int m = 0; m < POINTERS_PER_BLOCK; m++ ) {
	    
			if (ind.Pointers[m]) {
			    //printf("%u\n", ind.Pointers[m]);
			    this->d_bitmap[ind.Pointers[m]- (inodes + 1)] = 1;
			}
		    }
		}
	    }
	}
    }
    for (unsigned int l = 0; l < size - inodes - 1; l++) {
	//printf("%d\n", static_cast<int>(this->d_bitmap[l]));
    }

    this->inode_size = INODES_PER_BLOCK * inodes;

    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t FileSystem::create() {
    if(!d) return -1;

    // Locate free inode in inode table
    for (unsigned int i = 0; i < this->inode_size ; i++ ) {
	// Record inode if found
	if (!this->i_bitmap[i]) {
	    Block b;
	    d->read(i/INODES_PER_BLOCK + 1, b.Data);
	    b.Inodes[i % INODES_PER_BLOCK].Valid = 1; 
	    b.Inodes[i % INODES_PER_BLOCK].Size = 0;
	    d->write(i/INODES_PER_BLOCK + 1, b.Data);
	    this->i_bitmap[i] = 1;
	    return i;
	}
    }

    // No Inodes left 
    return -1;
}

// Remove inode ----------------------------------------------------------------
bool FileSystem::remove(size_t inumber) {
    
    if(!d) return false; 

    // Load inode information
    ssize_t inode_block = inumber / INODES_PER_BLOCK;
    ssize_t inode_index = inumber % INODES_PER_BLOCK;
    Block block;
    d->read(inode_block + 1, block.Data);
    char empty[Disk::BLOCK_SIZE]; 
    memset(empty, 0, Disk::BLOCK_SIZE+1);
    // Free direct blocks
    for (unsigned int i = 0; i < POINTERS_PER_INODE; i++) {
	uint32_t addr = block.Inodes[inode_index].Direct[i]; 
	if (addr) {
	    //printf("%u*%u\n", addr, addr- 1 - this->inode_blocks);
	    d->write(addr, empty);
	    this->d_bitmap[addr - 1 - this->inode_blocks ] = 0;
	    block.Inodes[inode_index].Direct[i] = 0;
	}
    }
    // Free indirect blocks
    uint32_t ind_addr = block.Inodes[inode_index].Indirect;
    if (ind_addr) {
	d->read(ind_addr, block.Data);
	for (unsigned int j = 0; j < POINTERS_PER_BLOCK; j++) {
	    if(block.Pointers[j]) {
		this->d_bitmap[block.Pointers[j]- 1 - this->inode_blocks] = 0;
		d->write(block.Pointers[j], empty);
		block.Pointers[j] =  0;
	    }
	}
	d->write(ind_addr, empty);
    }

    block.Inodes[inode_index].Size = 0;
    block.Inodes[inode_index].Valid= 0; 
    d->write(inode_block + 1, block.Data);
    
    // Clear inode in inode table
    i_bitmap[inumber] = 0;

    return true;
}

// Inode stat ------------------------------------------------------------------

ssize_t FileSystem::stat(size_t inumber) {
    // Load inode information
    if (!d) return -1;

    ssize_t inode_block = inumber / INODES_PER_BLOCK;
    ssize_t inode_index = inumber % INODES_PER_BLOCK;
    Block block;
    d->read(inode_block + 1, block.Data);
   
    if (block.Inodes[inode_index].Valid) return  block.Inodes[inode_index].Size; 
    else return -1;
}

// Read from inode -------------------------------------------------------------

ssize_t FileSystem::read(size_t inumber, char *data, size_t length, size_t offset) {
    if(!d) return -1;
    // Load inode information
    ssize_t inode_block = inumber / INODES_PER_BLOCK;
    ssize_t inode_index = inumber % INODES_PER_BLOCK;
    
    Block block;
    d->read(inode_block + 1, block.Data);
    uint32_t size = block.Inodes[inode_index].Size; 
    // Adjust length
    size_t block_start = offset / Disk::BLOCK_SIZE;
    size_t block_offset= offset % Disk::BLOCK_SIZE;
    // Read block and copy to data
    uint32_t x; 
    if (block_start >= POINTERS_PER_INODE && block.Inodes[inode_index].Indirect) {
	d->read(block.Inodes[inode_index].Indirect, block.Data);
	x = block.Pointers[block_start - POINTERS_PER_INODE]; 
    } else if (block_start >= POINTERS_PER_INODE && !block.Inodes[inode_index].Indirect) { 
	x = 0;
    } else {
	x = block.Inodes[inode_index].Direct[block_start];
    }
    
    if(!x) return 0;
    d->read(x, block.Data);
    char * s = (char *) calloc(Disk::BLOCK_SIZE + 10, sizeof(char));
    size_t i = 0;
    for(; i < Disk::BLOCK_SIZE; i++) {
	if(block.Data[i] != '\0') {
	    s[i] = block.Data[i];
	} else {
	    break;
	}
    }

    if (i == block_offset) {
	free(s);
	return 0;
    }
    std::string str(s);
    
    // Read Direct Blocks
    strcpy(data, str.c_str());
    if(size < (offset + i)) {
	data[block.Inodes[inode_index].Size] ='\0';
	free(s);
	return (block.Inodes[inode_index].Size);
    }
    
    free(s);
    return i;
    
}

// Write to inode --------------------------------------------------------------

ssize_t FileSystem::write(size_t inumber, char *data, size_t length, size_t offset, const char * name) {
    // Load inode
     if(!d) return -1;
    // Load inode information
    ssize_t inode_block = inumber / INODES_PER_BLOCK;
    ssize_t inode_index = inumber % INODES_PER_BLOCK;
    
    Block block;
    d->read(0, block.Data);
    uint32_t blocks = block.Super.Blocks;
    uint32_t inode_blocks = block.Super.InodeBlocks;

    d->read(inode_block + 1, block.Data);
    
    // Adjust length
    size_t block_start = offset / Disk::BLOCK_SIZE;
    size_t block_offset= offset % Disk::BLOCK_SIZE;
    
    // Read block and copy to data
    uint32_t x; 
    
    ssize_t result = 0;
    
    if(block_offset && block_start) {
	if (block_start >= POINTERS_PER_INODE && block.Inodes[inode_index].Indirect) {
	    d->read(block.Inodes[inode_index].Indirect, block.Data);
	    x = block.Pointers[block_start - POINTERS_PER_INODE]; 
	} else {
	    x = block.Inodes[inode_index].Direct[block_start];
	}
	Block newD;
	d->read(x, newD.Data);
	unsigned int k = block_offset;
	for(; k < Disk::BLOCK_SIZE; k++) {
	    if(data[k-offset] != '\0') {
		newD.Data[k] = data[k-block_offset];
		result++;
	    }
	}
	d->write(x, newD.Data);
	if(strlen(newD.Data) == Disk::BLOCK_SIZE) {
	    block_start++;
	    block_offset = 0;
	}
    }

    unsigned int new_d_blocks = length / Disk::BLOCK_SIZE;
    new_d_blocks = length % Disk::BLOCK_SIZE != 0 ?new_d_blocks + 1:new_d_blocks;
    memcpy(&block.Inodes[inode_index].Name, name, strlen(name)); 
    unsigned int i = 0;
    char cpy[Disk::BLOCK_SIZE + 1] = {0};

    for (unsigned int k = 0; k < Disk::BLOCK_SIZE + 1; k++) {
	cpy[k] = '\0';
    }
    for(unsigned int j = 0; j < new_d_blocks; j++) {
	
	for (; i < blocks-1-inode_blocks; i++ ) {
	    // Record data block if found
	    if (!this->d_bitmap[i]) {
		
		this->d_bitmap[i] = 1;
		
		if (j+block_start<POINTERS_PER_INODE) {
		    	
		    block.Inodes[inode_index].Direct[block_start+j] = i + 1 + inode_blocks;
		    if(length <= Disk::BLOCK_SIZE) {
			strncpy(cpy, data + result, length);
			result += length;
			cpy[length] = '\0';
			d->write(i+1 +inode_blocks, cpy);
			block.Inodes[inode_index].Size = result;
			d->write(inode_block+1, block.Data);
			return result;
		    } else {
			strncpy(cpy, data + result, Disk::BLOCK_SIZE);
			cpy[Disk::BLOCK_SIZE] = '\0';
			result += Disk::BLOCK_SIZE;
			length -= Disk::BLOCK_SIZE;
			block.Inodes[inode_index].Size = result;
			d->write(inode_block + 1, block.Data);
			d->write(i+1+inode_blocks,cpy);
		    }
		// Indirect pointers
		} else if (j+block_start >= POINTERS_PER_INODE) {
		    if (!block.Inodes[inode_index].Indirect) {
			block.Inodes[inode_index].Indirect = i + inode_blocks + 1;
			d->write(inode_block+1, block.Data);
			j-=1;
		    } else {
			Block ind;
			d->read(block.Inodes[inode_index].Indirect, ind.Data);
			ind.Pointers[j+block_start - POINTERS_PER_INODE] = i + inode_blocks + 1;
			
			if(length <= Disk::BLOCK_SIZE) {
			    strncpy(cpy, data + result, length);
			    result += length;
			    cpy[length] = '\0';
			    d->write(i+1 +inode_blocks, cpy);
			    block.Inodes[inode_index].Size = result;
			    d->write(inode_block + 1, block.Data);
			    d->write(block.Inodes[inode_index].Indirect, ind.Data);
			    return result;
			} else {
			    strncpy(cpy, data + result, Disk::BLOCK_SIZE);
			    cpy[Disk::BLOCK_SIZE] = '\0';
			    result += Disk::BLOCK_SIZE;
			    length -= Disk::BLOCK_SIZE;
			    block.Inodes[inode_index].Size = result;
			    d->write(inode_block + 1, block.Data);
			    d->write(block.Inodes[inode_index].Indirect, ind.Data);
			    d->write(i+1+inode_blocks,cpy);
			}
		    }

		}
		break;
	    }
	}
    }
    
    // Write block and copy to data
    return result;
}
